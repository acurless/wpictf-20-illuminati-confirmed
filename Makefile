CC = g++
CFLAGS = -Wall -Werror -Wextra -O2 -fPIC -g
LINK = -lgmpxx -lgmp

OBJECT_DIR = obj
INCLUDE =
SRC = $(wildcard *.cpp)

OBJ = $(patsubst %.cpp,$(OBJECT_DIR)/%.o,$(SRC))

ARTIFACT = crt

all: src

$(OBJECT_DIR)/%.o: %.cpp
	$(CC) -c $(INCLUDE) -o $@ $< $(CFLAGS)

$(ARTIFACT): $(OBJ)
	$(CC) -o $@ $^ $(LINK)

init:
	@mkdir -p $(OBJECT_DIR)

src: init $(ARTIFACT)

clean:
	rm -rf $(OBJECT_DIR) $(ARTIFACT)
